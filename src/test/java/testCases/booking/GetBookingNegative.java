package testCases.booking;

import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

public class GetBookingNegative {

    @Test(description = "Invalid booking retrieval scenario with empty Id and date")
    public void getBookingWithEmptyIdAndDate(){
        Response res =
            given()
            .when()
                .get("http://127.0.0.1:8900/booking")
            .then()
                .statusCode(400)
                .log().body()
                .extract().response();
        String jsonString = res.asString();
        Assert.assertEquals(jsonString, "Bad request: date and id empty");
    }

    @Test(description = "Invalid booking retrieval scenario with wrong date")
    public void getBookingWithWrongDate(){
        Response res =
            given()
            .when()
                .get("http://127.0.0.1:8900/booking?date=20200404")
            .then()
                .statusCode(500)
                .log().body()
                .extract().response();
        String jsonString = res.asString();
        Assert.assertEquals(jsonString.contains("Format date not valid"), true);
    }
}
