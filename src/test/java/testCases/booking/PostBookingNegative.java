package testCases.booking;

import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.HashMap;

import static io.restassured.RestAssured.given;

public class PostBookingNegative {

    @Test(description = "Invalid booking creation scenario with incorrect date")
    public void postBookingWithWrongDate(){

        HashMap data = new HashMap();
        data.put("date", "202109-12");
        data.put("destination", "WRO");
        data.put("id", "newuser1@gmail.com-0.05264734032895546");
        data.put("origin", "WAW");

        Response res =
            given()
                .contentType("application/json")
                .body(data)
            .when()
                .post("http://127.0.0.1:8900/booking")
            .then()
                .statusCode(400)
                .log().body()
                .extract().response();
        String jsonString = res.asString();
        Assert.assertEquals(jsonString, "Date format not valid");
    }

    @Test(description = "Invalid booking creation scenario with empty date")
    public void postBookingWithEmptyDate(){

        HashMap data = new HashMap();
        data.put("date", "");
        data.put("destination", "WRO");
        data.put("id", "newuser1@gmail.com-0.05264734032895546");
        data.put("origin", "WAW");

        Response res =
            given()
                .contentType("application/json")
                .body(data)
            .when()
                .post("http://127.0.0.1:8900/booking")
            .then()
                .statusCode(400)
                .log().body()
                .extract().response();
        String jsonString = res.asString();
        Assert.assertEquals(jsonString, "Date format not valid");
    }

    @Test(description = "Invalid booking creation scenario with incorrect destination")
    public void postBookingWithWrongDestination(){

        HashMap data = new HashMap();
        data.put("date", "2021-09-11");
        data.put("destination", "WRO-");
        data.put("id", "newuser1@gmail.com-0.05264734032895546");
        data.put("origin", "WAW");

        Response res =
            given()
                .contentType("application/json")
                .body(data)
            .when()
                .post("http://127.0.0.1:8900/booking")
            .then()
                .statusCode(409)
                .log().body()
                .extract().response();
        String jsonString = res.asString();
        Assert.assertEquals(jsonString, "Origin or Destination is not a IATA code (Three Uppercase Letters)");
    }

    @Test(description = "Invalid booking creation scenario with empty destination")
    public void postBookingWithEmptyDestination(){

        HashMap data = new HashMap();
        data.put("date", "2021-09-11");
        data.put("destination", "");
        data.put("id", "newuser1@gmail.com-0.05264734032895546");
        data.put("origin", "WAW");

        Response res =
            given()
                .contentType("application/json")
                .body(data)
            .when()
                .post("http://127.0.0.1:8900/booking")
            .then()
                .statusCode(409)
                .log().body()
                .extract().response();
        String jsonString = res.asString();
        Assert.assertEquals(jsonString, "Origin or Destination is not a IATA code (Three Uppercase Letters)");
    }

    @Test(description = "Invalid booking creation scenario with incorrect Id")
    public void postBookingWithWrongId(){

        HashMap data = new HashMap();
        data.put("date", "2021-09-11");
        data.put("destination", "WRO");
        data.put("id", "newuser1gmail.com-0.05264734032895546");
        data.put("origin", "WAW");

        Response res =
            given()
                .contentType("application/json")
                .body(data)
            .when()
                .post("http://127.0.0.1:8900/booking")
            .then()
                .statusCode(500)
                .log().body()
                .extract().response();
        String jsonString = res.asString();
        Assert.assertEquals(jsonString.contains("Internal Server Error"), true);
    }

    @Test(description = "Invalid booking creation scenario with empty Id")
    public void postBookingWithEmptyId(){

        HashMap data = new HashMap();
        data.put("date", "2021-09-11");
        data.put("destination", "WRO");
        data.put("id", "");
        data.put("origin", "WAW");

        Response res =
            given()
                .contentType("application/json")
                .body(data)
            .when()
                .post("http://127.0.0.1:8900/booking")
            .then()
                .statusCode(400)
                .log().body()
                .extract().response();
        String jsonString = res.asString();
        Assert.assertEquals(jsonString, "User id should be valid");
    }

    @Test(description = "Invalid booking creation scenario with invalid origin")
    public void postBookingWithWrongOrigin(){

        HashMap data = new HashMap();
        data.put("date", "2021-09-11");
        data.put("destination", "WRO");
        data.put("id", "newuser1@gmail.com-0.05264734032895546");
        data.put("origin", "WAW-");

        Response res =
            given()
                .contentType("application/json")
                .body(data)
            .when()
                .post("http://127.0.0.1:8900/booking")
            .then()
                .statusCode(409)
                .log().body()
                .extract().response();
        String jsonString = res.asString();
        Assert.assertEquals(jsonString, "Origin or Destination is not a IATA code (Three Uppercase Letters)");
    }

    @Test(description = "Invalid booking creation scenario with empty origin")
    public void postBookingWithEmptyOrigin(){

        HashMap data = new HashMap();
        data.put("date", "2021-09-11");
        data.put("destination", "WRO");
        data.put("id", "newuser1@gmail.com-0.05264734032895546");
        data.put("origin", "");

        Response res =
            given()
                .contentType("application/json")
                .body(data)
            .when()
                .post("http://127.0.0.1:8900/booking")
            .then()
                .statusCode(409)
                .log().body()
                .extract().response();
        String jsonString = res.asString();
        Assert.assertEquals(jsonString, "Origin or Destination is not a IATA code (Three Uppercase Letters)");
    }
}
