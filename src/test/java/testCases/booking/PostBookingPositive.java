package testCases.booking;

import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;
import testCases.user.PostUserPositive;
import java.util.HashMap;
import static io.restassured.RestAssured.given;

public class PostBookingPositive {

    @Test(description = "Valid booking creation scenario with correct data")
    public void postBooking(){

        HashMap data = new HashMap();
        data.put("date", "2021-09-12");
        data.put("destination", "WRO");
        data.put("id", PostUserPositive.getUserId());
        data.put("origin", "WAW");

        Response res =
            given()
                .contentType("application/json")
                .body(data)
            .when()
                .post("http://127.0.0.1:8900/booking")
            .then()
                .statusCode(201)
                .log().body()
                .extract().response();
        String jsonString = res.asString();
        String origin = res.path("origin");
        String destination = res.path("destination");
        String idBooking = origin + "-" + destination;
        Assert.assertEquals(jsonString.contains(idBooking), true);
    }
}
