package testCases.user;

import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.given;

public class GetUserNegative {

    @Test(description = "Invalid user retrieval scenario with incorrect Id")
    public void getUserWithWrongId(){
        Response res =
            given()
            .when()
                .get("http://127.0.0.1:8900/user?id=test")
            .then()
                .statusCode(404)
                .log().body()
                .extract().response();
        String jsonString = res.asString();
        Assert.assertEquals(jsonString, "User not found");
    }

    @Test(description = "Invalid user retrieval scenario with empty Id")
    public void getUserWithEmptyId(){
        Response res =
            given()
            .when()
                .get("http://127.0.0.1:8900/user")
            .then()
                .statusCode(500)
                .log().body()
                .extract().response();
        String jsonString = res.asString();
        Assert.assertEquals(jsonString, "ID erroneus");
    }
}
