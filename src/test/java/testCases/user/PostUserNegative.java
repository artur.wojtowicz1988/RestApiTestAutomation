package testCases.user;

import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.util.HashMap;
import static io.restassured.RestAssured.given;

public class PostUserNegative {

    @Test(description = "Invalid create user scenario with empty name")
    public void postUserWithEmptyName(){

        HashMap data = new HashMap();
        data.put("email", "newuser1@gmail.com");
        data.put("name", "");
        //@formatter:off
        Response res =
                given()
                        .contentType("application/json")
                        .body(data)
                        .when()
                        .post("http://127.0.0.1:8900/user")
                        .then()
                        .statusCode(409)
                        .log().body()
                        .extract().response();
        String jsonString = res.asString();
        Assert.assertEquals(jsonString, "Check fields");

    }

    @Test(description = "Invalid create user scenario with empty email")
    public void postUserWithEmptyEmail(){

        HashMap data = new HashMap();
        data.put("email", "newuser1@gmail.com");
        data.put("name", "");
        //@formatter:off
        Response res =
            given()
                .contentType("application/json")
                .body(data)
            .when()
                .post("http://127.0.0.1:8900/user")
            .then()
                .statusCode(409)
                .log().body()
                .extract().response();
        String jsonString = res.asString();
        Assert.assertEquals(jsonString, "Check fields");

    }
    @Test(description = "Invalid create user scenario with empty name and email")
    public void postUserWithEmptyNameAndEmail(){

        HashMap data = new HashMap();
        data.put("email", "");
        data.put("name", "");

        Response res=
            given()
                .contentType("application/json")
                .body(data)
            .when()
                .post("http://127.0.0.1:8900/user")
            .then()
                .statusCode(409)
                .log().body()
                .extract().response();
        String jsonString = res.asString();
        Assert.assertEquals(jsonString, "Check fields");

    }
}
